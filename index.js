//npm init -y  -skips all prompts & goes directly to loading the package.JSON
//npm install express
//npm install mongoose
//to avoid killing and refreshing port on gitbash, insert:
	//"start": "node index.js" => on package.json; then on gitbash, only input: npm start (instead of npm index.js)

//MINIACTIVITY
const express = require("express");

//mongoose - pkg that allows creation of schemas to model the data structure & to have access to a number of methods for manipulating concrete...
const mongoose = require("mongoose");

const app = express();

const port = 3000;


mongoose.connect("mongodb+srv://clarissacristobal:clarissacristobal@wdc028-course-booking.s6ug6.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})


// notification for connection: success/failure
let db = mongoose.connection;
// if an error existed in connecting to MongoDB
db.on("error", console.error.bind(console, "Connection Error"));
// if the connection is successful
db.once("open", () => console.log("We're connected to the database"))





app.use(express.json());//lets app read json data
app.use(express.urlencoded({extended: true})); //allows app to receive data from forms


//Mongoose Schema - sets the structure of doc that is to be created; serves as the blueprint to the data/record
const taskSchema = new mongoose.Schema({
	name: String,
	status:{
		type: String,
		//default - sets the value once the field does not have any value entered in it
		default: "pending"
	}
})

//model - allows access to methods that will perform CRUD operations in database; RULE: the 1st letter is always capital/uppercase for the variable of the model & must be singular form
/*
	SYNTAX:
	const <Variable> = mongoose.model("<Variable>", <schemaName>)
*/
const Task = mongoose.model("Task", taskSchema)

//routes
/*
business logic:
1. check if task is already existing
	if task exists, return "there is a duplicate task"
	if not existing, add task in the database

2. the task will come from request body

3. create a new task object w the needed properties

4. save to db 
*/

app.post("/tasks", (req, res) => {
	//checking for duplicate tasks
	//findOne is a mongoose method that acts similar to find in MongoDb; returns the first doc it finds
	Task.findOne({name:req.body.name}, (error, result) => {
		//if "result" has found a task, it will return the res.send
		if(result !== null && result.name === req.body.name){
			return res.send("There is a duplicate task")
		} else {
			let newTask = new Task ({
				name: req.body.name
			})
			//saveErr - parameter that accepts errors, if any, when saving "newTask" object
			//savedTask - parameter that accepts the object should the saving of "newTask" object is a success
			newTask.save((saveErr, savedTask) => {
				//if there are errors, log in the console the error
				if (saveErr){
					return console.error(saveErr);
				} else {
					//.status - returns a status (num code such as 201 for successful creation)
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})


app.get("/tasks", (req, res) => {
	//find is similar to mongodb/robo3t find. setting up field in "{}" would allow app to find all docs in database
	Task.find({}, (error, result) => {
		if (error){
			return console.log(error)
		}else{
			return res.status(200).json({data:result}) //data: field will ve set & its value will be result/response that we had for the request
		}
	})
})



// create a function/route for a user registration
/*
1. Find if there is a duplicate user using the username field
	- if the user is existing, return "username already in use"
	- if the user is not existing, add it in the database
		- if the username and the password are filled, save
			- in saving, create a new User object (a User schema)
				- if there is an errror, log it in the console
				- if there is no error, send a status of 201 and "successfully registered"
		- if one of them is blank, send the response "BOTH username and password must be provided"
*/

//Mongoose Schema - sets the structure of doc that is to be created; serves as the blueprint to the data/record
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema)

app.post("/signup", (req, res) => {
	//checking for duplicate users
	//findOne is a mongoose method that acts similar to find in MongoDb; returns the first doc it finds
	User.findOne({username:req.body.username}, (error, result) => {
		//if "result" has found a task, it will return the res.send
		if(result !== null && result.username === req.body.username){
			return res.send("Username already in use")
		} 
		if(req.body.username == '' || req.body.password == ''){
			return res.send("BOTH username and password must be provided")
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})
			//saveErr - parameter that accepts errors, if any, when saving "newTask" object
			//savedTask - parameter that accepts the object should the saving of "newTask" object is a success
			newUser.save((saveErr, savedTask) => {
				//if there are errors, log in the console the error
				if (saveErr){
					return console.error(saveErr);
				} else {
					//.status - returns a status (num code such as 201 for successful creation)
					return res.status(201).send("Successfully registered")
				}
			})
		}
	})
})


// create a get request for getting all registered users
/*
	- retrieve all users registerd using find functionality
	- if there is an error, log it in the console
	- if there is no error, send a success status back to the client and return the array of users
*/

app.get("/users", (req, res) => {
	//find is similar to mongodb/robo3t find. setting up field in "{}" would allow app to find all docs in database
	User.find({}, (error, result) => {
		if (error){
			return console.log(error)
		}else{
			return res.status(200).json({data:result}) //data: field will ve set & its value will be result/response that we had for the request
		}
	})
})





app.listen(port, () => console.log(`Server is running at port ${port}`)); //must be only 1
